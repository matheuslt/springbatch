package br.com.matheuslehnen.springbatch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job job() {
        // return this.jobBuilderFactory.get("jobQuartz").incrementer(new RunIdIncrementer()).start(step1()).build();

        return jobBuilderFactory.get("jobQuartz")
                .incrementer(new RunIdIncrementer()).listener(listener())
                .flow(step1()).end().build();
    }

    @Bean
    public Step step1() {
        /*return this.stepBuilderFactory.get("step1").tasklet((stepContribution, chunkContext) -> {
            System.out.println("step1 ran!");
            return RepeatStatus.FINISHED;
        }).build();*/
        return stepBuilderFactory.get("orderStep1").<String, String> chunk(1)
                .reader(new Reader()) // Classe nova com as listas (DevConsole e BD)
                .processor(new Processor()) // novosRegistros()
                    .writer(new Writer()) // inserir()
                .processor(new Processor()) // registrosModificados()
                    .writer(new Writer()) // atualizar()
                .processor(new Processor()) // registrosDeletados()
                    .writer(new Writer()) // deletar()
                .build();
    }

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionListener();
    }
}